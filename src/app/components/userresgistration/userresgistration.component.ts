import { environment } from './../../../environments/environment';
import { Component, Input, OnInit } from '@angular/core';
import { UsersService } from './../../services/users.service';
import { UserModel } from './../../models/user/user.model';
import { Response } from './../../models/response';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-userresgistration',
  templateUrl: './userresgistration.component.html',
  styleUrls: ['./userresgistration.component.scss']
})
export class UserresgistrationComponent implements OnInit {

  @Input() userEdit;
  public user = {
    name: null,
    email: null,
    code: '+54',
    phone: null,
    position: 'pastor',
    branch:'Obera',
    pass: null,
    passDecrypted: null,
    permissions: null,
  }
  public internationalCode = [
    '+54','+55','+59'
  ]
  public operators = [
    'pastor',
    'contador',
    'revisor',
  ]
  public branchs = [
    'Aristobulo',
    'Brasil',
    'Campo Grande',
    'Campo Viera',
    'Colonia Tacuara',
    'L. N. Alem',
    'Loreto',
    'Obera',
    'Panambi',
    'Paraguay',
    'Posadas',
    'San Javier'
  ]
  public textError = null;

  public textResponse = null;

  private clave = environment.clave;

  constructor( private users: UsersService) { }

  get jsonFormatter() {
    return {
      stringify: (cipherParams: any) => {
        const jsonObj = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64), iv: null, s: null };
        if (cipherParams.iv) {
          jsonObj.iv = cipherParams.iv.toString();
        }
        if (cipherParams.salt) {
          jsonObj.s = cipherParams.salt.toString();
        }
        return JSON.stringify(jsonObj);
      },
      parse: (jsonStr) => {
        const jsonObj = JSON.parse(jsonStr);
        // extract ciphertext from json object, and create cipher params object
        const cipherParams = CryptoJS.lib.CipherParams.create({
          ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct)
        });
        if (jsonObj.iv) {
          cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv);
        }
        if (jsonObj.s) {
          cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s);
        }
        return cipherParams;
      }
    };
  }

  ngOnInit(): void {
  }

  registerNewUser(){

    this.user.pass = this.encrypt(this.user.pass);

    this.users.altaUsuario(this.user).subscribe(
      res => {
        const text = res as Response;
        this.textResponse = text.mensaje;
      },
      error =>{
        console.log(error);
      }
    );
  }
  getTitle(){
    if (this.userEdit === null){
      return 'Crear Usuario';
    }else{
      this.user = this.userEdit;
      // console.log(this.user);
      return 'Editando Datos de ' + this.user.name;
    }
  }
  getTypePass(){
    if (this.userEdit === null){
      return "password";
    }else{
      return "text";
    }
  }
  getTitleButton(){
    if (this.userEdit === null){
      return "Crear Usuario";
    }else{
      return "Modificar Usuario";
    }
  }
  analizeEmail(email){
    this.users.obtenerUsuarios().subscribe(
      res => {
        const users = res as UserModel;
        this.textError = checkEmail(users, email);
      },
      error => {
        console.log(error);
      }
    );

    function checkEmail(users, email){
      for (const user of users){
        console.log(user.email);

        if (user.email === email){
          return 'El email ya está registrado';
        }else{
          return null;
        }
      }
    }
  }
  analizePass(){
    if (this.user.pass !== null
      && this.user.passDecrypted !== null
      && this.user.pass === this.user.passDecrypted){
      return false;
    }else if(this.user.pass === null
      || this.user.passDecrypted === null
      || this.user.pass === this.user.passDecrypted){
        return true;
    }
  }

  /* Method for Encryption */
  encrypt(value: any) {
    const encrypted = CryptoJS.AES.encrypt(value, this.clave,
      { format: this.jsonFormatter, mode: CryptoJS.mode.CBC }).toString();
    this.decrypt(encrypted);
    return encrypted;
  }
  /* Method for Decryption */
  decrypt(value: any): any {
    if (value){
    const key = this.clave; // SECRET KEY FOR ENCRYPTION
    const decrypted = CryptoJS.AES.decrypt(value, key, {format: this.jsonFormatter }).toString(CryptoJS.enc.Utf8);
    return decrypted;
    }
  }

}
