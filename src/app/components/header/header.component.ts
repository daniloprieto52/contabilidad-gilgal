import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() user;
  @Input() branchSelect;

  public closePopUp = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  closePopup(){
    if (this.closePopUp === true){
      this.closePopUp = false;
    }else{
      this.closePopUp = true
    }
  }

  closeSession(){
    localStorage.removeItem('user');
    this.router.navigateByUrl('login');
  }


}
