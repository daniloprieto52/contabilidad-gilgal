import { OrdersService } from './../../services/orders.service';
import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
@Component({
  selector: 'app-cash-register',
  templateUrl: './cash-register.component.html',
  styleUrls: ['./cash-register.component.scss']
})
export class CashRegisterComponent implements OnInit {

  @Input() branchSelect;
  @Input() user;
  @Input() orders;
  @Input() ingressMonth;
  @Input() egressMonth;
  @Input() tithesMonth;
  @Input() offeringsMonth;
  @Input() donationsMonth;
  @Input() available;
  @Input() shepherdProfit;
  @Input() ingressBalance;
  @Input() egressBalance;
  @Input() tithesBalance;
  @Input() offeringsBalance;
  @Input() donationsBalance;
  @Input() availableBalance;
  @Input() shepherdProfitBalance;

  @Output() reloadOrders: EventEmitter<any> = new EventEmitter<any>();

  public input = true;
  public action = 'ingress';
  public history = false;
  public showPayShepherd=false;
  public showAvailableDetail=false;
  public showIngressDetail=false;
  public showEgressDetail=false;
  public ordersMonth = null;


  constructor(private orderService: OrdersService) { }

  ngOnInit(): void {
    this.getOrdersMonth();
  }

  showInput(action: string){
    this.input = true;
    this.action = action;
    this.history = false;
  }

  payShepherd(){

    let order = {
      action: 'pastor',
      motive: 'Pago al Pastor',
      branch: String(this.branchSelect),
      operator: String(this.user.name),
      amount: Number(this.shepherdProfit),
    }

    this.orderService.generateOrder(order).subscribe(
      res =>{
        console.log(res);
      },
      error => {
        console.log(error);
      }
    );
    this.reloadData(true);
  }

  getMonthName(){
    let  months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septimbre", "Octubre", "Noviembre", "Diciembre"];
    let d = new Date();
    let monthName = months[d.getMonth()];
    return monthName + ' ' + d.getFullYear();
  }

  getOrdersMonth(){
    const d  = new Date();
    let ordersMonth = [];
    for(const order of this.orders){
      const dateOrder = new Date(order.date);
      if (dateOrder.getFullYear() === d.getFullYear() && dateOrder.getMonth() === d.getMonth()){
        ordersMonth.push(order);
      }
    }

    this.ordersMonth = ordersMonth;
  }

  reloadData(e:boolean){
    this.reloadOrders.emit(e);
  }

}
