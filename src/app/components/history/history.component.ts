import { OrdersService } from './../../services/orders.service';
import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  @Input() branchSelect;
  @Input() orders;

  constructor(private ordersService: OrdersService) { }

  ngOnInit(): void {
  }



  getDate(date){
    const d = new Date(date);
    return d.getDay() + '/' + d.getMonth() + '/' + d.getFullYear();
  }

}
