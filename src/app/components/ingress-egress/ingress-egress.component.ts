import { OrdersService } from './../../services/orders.service';
import { Order } from './../../models/order';
import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import * as printJS from 'print-js';
@Component({
  selector: 'app-ingress-egress',
  templateUrl: './ingress-egress.component.html',
  styleUrls: ['./ingress-egress.component.scss']
})
export class IngressEgressComponent implements OnInit {

  @Input() action;
  @Input() user;
  @Input() branchSelect;

  @Output() reloadOrders: EventEmitter<any> = new EventEmitter<any>();


  public order = {
    action: null,
    motive: null,
    amount: null,
    taxpayer: null,
    branch: null,
    operator: null,
   }

  public orderPrint = null;

  constructor( private orderService: OrdersService) { }

  ngOnInit(): void {
  }
  getMotive(){
    if(this.action === 'ingress'){
      return 'Ingreso';
    }else if ( this.action === 'egress'){
      return 'Egreso';
    }
  }

  charge(){
    this.order.action = String(this.action);
    this.order.branch = String(this.branchSelect);
    this.order.operator = String(this.user.name);
    this.order.amount = Number(this.order.amount);

    let form = new FormData();

    form.append('action', String(this.order.action));
    form.append('motive', String(this.order.motive));
    form.append('amount', this.order.amount);
    form.append('taxpayer', String(this.order.taxpayer));
    form.append('branch', String(this.order.branch));
    form.append('operator', String(this.order.operator));
    console.log(form);

    this.orderService.generateOrder(this.order).subscribe(
      res =>{
        console.log(res);
        if (res){
          this.orderPrint = res;
          this.order = {
              action: null,
              motive: null,
              amount: null,
              taxpayer: null,
              branch: null,
              operator: null,
          }
          this.reloadData(true);
        }

      },
      error => {
        console.log(error);
      }
    );
  }

  print(){
    printJS('orderPrint', 'html');
    this.orderPrint = null;
  }

  reloadData(e:boolean){
    this.reloadOrders.emit(e);
  }

  getTime(){
    const t = new Date();
    return t.getHours() + ':' + t.getMinutes();
  }

}
