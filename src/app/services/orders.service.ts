import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  URL = environment.url;

  constructor(private http: HttpClient) { }

  retrieveAllOrders() {
    return this.http.get(`${this.URL}retrieveOrders.php`);
  }

  generateOrder(order) {
    return this.http.post(`${this.URL}generateOrder.php`, JSON.stringify(order));
  }
}
