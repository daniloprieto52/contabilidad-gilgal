export class Order {
  constructor(
    id?: number,
    date?: any,
    action?: string,
    motive?: string,
    amount?: number,
    taxpayer?: string,
    branch?: string,
    operator?: string,
  ){}
}
