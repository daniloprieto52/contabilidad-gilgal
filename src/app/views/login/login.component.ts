import { Component, OnInit } from '@angular/core';
import { UsersService } from './../../services/users.service';
import { Router } from '@angular/router';
import { UserModel } from './../../models/user/user.model';
import { environment } from './../../../environments/environment';
import * as CryptoJS from 'crypto-js';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user = {
    user: null,
    pass: null,
  };
  public textError = null;

  private clave = environment.clave;

  constructor(  private users: UsersService,
                public router: Router) { }

  get jsonFormatter() {
    return {
      stringify: (cipherParams: any) => {
        const jsonObj = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64), iv: null, s: null };
        if (cipherParams.iv) {
          jsonObj.iv = cipherParams.iv.toString();
        }
        if (cipherParams.salt) {
          jsonObj.s = cipherParams.salt.toString();
        }
        return JSON.stringify(jsonObj);
      },
      parse: (jsonStr) => {
        const jsonObj = JSON.parse(jsonStr);
        // extract ciphertext from json object, and create cipher params object
        const cipherParams = CryptoJS.lib.CipherParams.create({
          ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct)
        });
        if (jsonObj.iv) {
          cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv);
        }
        if (jsonObj.s) {
          cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s);
        }
        return cipherParams;
      }
    };
  }

  ngOnInit(): void {
  }

  retrieveUsers(){
    this.users.obtenerUsuarios().subscribe(
      res => {
        // console.log(res as UserModel);
        const usersList = res as UserModel;
        this.analizeUser(usersList);
      },
      error => {
        console.log(error);
      }
    );
  }

  analizeUser(usersList){
    let exist = false;
    for(const user of usersList){

      if (user.email === this.user.user){
        exist = true

        if (this.decrypt(user.pass) === this.user.pass){
          localStorage.setItem('user', JSON.stringify(user));
          this.router.navigate(['//admin']);
          this.user.user = null;
          this.user.pass = null;
        }else{
          this.textError = 'Contraseña incorrecta';
          this.user.user = null;
          this.user.pass = null;
        }
      }
    }

    if(exist === false){
      this.textError = 'email incorrecto';
      this.user.user = null;
      this.user.pass = null;
    }

  }

  /* Method for Decryption */
  decrypt(value: any): any {
    if (value){
    const key = this.clave; // SECRET KEY FOR ENCRYPTION
    const decrypted = CryptoJS.AES.decrypt(value, key, {format: this.jsonFormatter }).toString(CryptoJS.enc.Utf8);
    return decrypted;
    }
  }

}
