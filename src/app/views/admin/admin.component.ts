import { Order } from './../../models/order';
import { OrdersService } from './../../services/orders.service';
import { UserModel } from './../../models/user/user.model';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public churchBranch = [
    'Caja Central',
    'Aristobulo',
    'Brasil',
    'Campo Grande',
    'Campo Viera',
    'Colonia Tacuara',
    'L. N. Alem',
    'Loreto',
    'Obera',
    'Panambi',
    'Paraguay',
    'Posadas',
    'San Javier'
  ]

  public select=null;
  public profile=true;
  public registration=false;
  public userEdit = null;
  public user = JSON.parse(localStorage.getItem('user')) as UserModel;
  public branchSelect = this.getBranch();
  public cashRegister=false;
  public orders: Order = null
  public ingressMonth = null;
  public egressMonth = null;
  public tithesMonth = null;
  public offeringsMonth = null;
  public donationsMonth = null;
  public available = null;
  public shepherdProfit = null;

  public ingressBalance = null;
  public egressBalance = null;
  public tithesBalance = null;
  public offeringsBalance = null;
  public donationsBalance = null;
  public availableBalance = null;
  public shepherdProfitBalance = null;

  constructor(private ordersService: OrdersService) { }

  ngOnInit(): void {
    this.retrieveOrders();
  }
  getBranch(){
    if (this.user.branch){
      return this.user.branch;
    }
  }

  retrieveOrders(){
    this.ordersService.retrieveAllOrders().subscribe(
      res => {
        this.orders = res as Order[];
        this.totalIngressMonth(res as Order[]);
        this.balancePerMonth(res as Order[]);
      },
      error => {
        console.log(error);
      }
    );
  }

  totalIngressMonth(orders){
    let ingress = [];
    let egress = [];
    let payedShepherd = [];

    //filtro las ordenes del mes actual y las divido en ingresos y egresos
    for(const order of orders){
      const d = new Date(order.date);
      const now = new Date();
      if (order.branch === this.branchSelect){
        if(d.getFullYear() === now.getFullYear() && d.getMonth() === now.getMonth()){
          if (order.action === 'ingress'){
            ingress.push(order);
          }else if (order.action === 'egress'){
            egress.push(order.amount);
          }else if (order.action === 'pastor'){
            payedShepherd.push(order.amount)
          }
        }
      }

    }

    //Separo los ingresos en diezmos, ofrendas y donaciones
    let tithe = [];
    let offering = [];
    let donation = [];

    for(let order of ingress){
      if(order.motive === 'diezmo'){
        tithe.push(order.amount);
      }else if (order.motive === 'ofrenda'){
        offering.push(order.amount);
      }else if (order.motive === 'donacion'){
        donation.push(order.amount);
      }
    }

    // sumo los valores de diezmos y ofrendas
    let totalTithes = 0;
    for(let i = 0; tithe.length > i; i++){
      totalTithes += Number(tithe[i]);
    }

    let totalOfferings = 0;
    for(let i = 0; offering.length > i; i++){
      totalOfferings += Number(offering[i]);
    }

    let totalDonations = 0;
    for(let i = 0; donation.length > i; i++){
      totalDonations += Number(donation[i]);
    }

    //Sumo para obtener el ingreso total
    this.ingressMonth = totalTithes + totalOfferings + totalDonations;
    this.tithesMonth = totalTithes;
    this.offeringsMonth = totalOfferings;
    this.donationsMonth = totalDonations;


    // Sumo los egresos
    let totalEgress = 0;
    for(let i = 0; egress.length > i; i++){
      totalEgress += Number(egress[i]);
    }

    // Obtengo el importe del pastor

    let payed = 0;
    for(let i = 0; payedShepherd.length > i; i++){
      payed += Number(payedShepherd[i]);
    }

    let shepherdProfit = ( (this.ingressMonth - this.donationsMonth) * 50/100 ) - ( ( (this.ingressMonth - this.donationsMonth) * 50/100 ) * 10/100 );
    this.shepherdProfit = shepherdProfit - payed;

    //Obtengo los egresos totales
    this.egressMonth = totalEgress + payed;

    // obtengo el monto disponible para gastos
    this.available = this.ingressMonth - (this.egressMonth + this.shepherdProfit);

  }

  balancePerMonth(orders){
    let ingress = [];
    let egress = [];
    let payedShepherd = [];

    //filtro las ordenes del mes actual y las divido en ingresos y egresos
    for(const order of orders){
      const d = new Date(order.date);
      const now = new Date();
      if (order.branch === this.branchSelect){
        if(d.getFullYear() === now.getFullYear() && d.getMonth() !== now.getMonth()){
          if (order.action === 'ingress'){
            ingress.push(order);
          }else if (order.action === 'egress'){
            egress.push(order.amount);
          }else if (order.action === 'pastor'){
            payedShepherd.push(order.amount)
          }
        }
      }

    }

    //Separo los ingresos en diezmos, ofrendas y donaciones
    let tithe = [];
    let offering = [];
    let donation = [];

    for(let order of ingress){
      if(order.motive === 'diezmo'){
        tithe.push(order.amount);
      }else if (order.motive === 'ofrenda'){
        offering.push(order.amount);
      }else if (order.motive === 'donacion'){
        donation.push(order.amount);
      }
    }

    // sumo los valores de diezmos y ofrendas
    let totalTithes = 0;
    for(let i = 0; tithe.length > i; i++){
      totalTithes += Number(tithe[i]);
    }

    let totalOfferings = 0;
    for(let i = 0; offering.length > i; i++){
      totalOfferings += Number(offering[i]);
    }

    let totalDonations = 0;
    for(let i = 0; donation.length > i; i++){
      totalDonations += Number(donation[i]);
    }

    //Sumo para obtener el ingreso total
    this.ingressBalance = totalTithes + totalOfferings + totalDonations;
    this.tithesBalance = totalTithes;
    this.offeringsBalance = totalOfferings;
    this.donationsBalance = totalDonations;


    // Sumo los egresos
    let totalEgress = 0;
    for(let i = 0; egress.length > i; i++){
      totalEgress += Number(egress[i]);
    }

    // Obtengo el importe del pastor

    let payed = 0;
    for(let i = 0; payedShepherd.length > i; i++){
      payed += Number(payedShepherd[i]);
    }

    let shepherdProfit = ( (this.ingressBalance - this.donationsBalance) * 50/100 ) - ( ( (this.ingressBalance - this.donationsBalance) * 50/100 ) * 10/100 );
    this.shepherdProfitBalance = shepherdProfit - payed;


    //Obtengo los egresos totales
    this.egressBalance = totalEgress + payed;

    // obtengo el monto disponible para gastos
    this.availableBalance = this.ingressBalance - (this.egressBalance + this.shepherdProfitBalance);

  }

  reloadOrders(e){
    this.orders = null
    this.ingressMonth = null;
    this.egressMonth = null;
    this.tithesMonth = null;
    this.offeringsMonth = null;
    this.donationsMonth = null;
    this.available = null;
    this.shepherdProfit = null;

    this.ingressBalance = null;
    this.egressBalance = null;
    this.tithesBalance = null;
    this.offeringsBalance = null;
    this.donationsBalance = null;
    this.availableBalance = null;
    this.shepherdProfitBalance = null;

    this.retrieveOrders();
  }



}
